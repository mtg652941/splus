# Mesecons Windows [`mesecons_window`]

This mod adds 3 glass blocks for the Mesecons modpack.
They change their transparency when powered with Mesecon energy.

They conduct energy to their 6 direct neighbors.

Version: 1.0.1

## Block transparency

    Block                   On       Off
    Mesecon Window          Full     None
    Mesecon Filter Glass    Full     Partial
    Mesecon Filter Window   Partial  None


## License of everything
MIT License
