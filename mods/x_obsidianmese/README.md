# [Mod] X Obsidianmese [x_obsidianmese]

Adds more use for Mese and Obsidian. This is the original mod from Survival X server.

![screenshot](screenshot.png)

## Items

### Sword

Fast, sharp and durable. Cuts through Mithril.

### Sword Engraved

Fast, sharp and durable. Cuts through Mithril. Shoots basic projectiles on right click (secondary click).

### Pickaxe

Fast, sharp and durable. Ideal for long mining - specially obsidian, without crafting multiple pickaxes.
Mines through sand and gravel easily for faster mining.

### Pickaxe Engraved

More durable than diamond pickaxe. Additionally it can place item from hotbar - next to the pickaxe to the right.
Ideal for building when you can place/dig blocks without switching selected item in hotbar or mining and placing torches.

### Axe

Will dig the whole tree at once. All trees with group `{ tree = 1 }` are compatible. Additionally cactus is supported also.

### Shovel

Right-click/secondary click will create paths on supported blocks (all dirt, sand, snow, permafrost from MTG are supported).

### Hoe

Regular use will plow 5 block in the direction you are looking. Right-click/secondary use will place 5 seeds in the direction you are looking - seeds have to be somewhere in your main inventory.
Additionally if you dig a hole (1 block deep), drop bucket (item from inventory) of water inside the hole (bucket has to be surrounded by blocks to prevent water leakage) then punch the water bucket with this hoe - a plowed field (3 block radius) will be created with water source in the middle (where the bucket was placed).

### Chest

This chest holds the same inventory for a player. You can store items in this chest and the same items will be available in different obsidianmese chest on a different place in the world.
Good for mining while carrying this chest - place the chest and store you items in it to free up some inventory space for more mining.
All stored items will be available in the same chest when you get back home.

### Apple

Heals full amount of health.

## Dependencies

- default

## Optional Dependencies

- x_farming
- ethereal

## License:

### Code

GNU Lesser General Public License v2.1 or later (see included LICENSE file)

### Textures

**LGPL-2.1-or-later, by SaKeL**

- x_obsidianmese_apple.png
- x_obsidianmese_desert_sand_path_side.png
- x_obsidianmese_chest_front.png
- x_obsidianmese_chest_top.png
- x_obsidianmese_sand_path_side.png
- x_obsidianmese_chest_side.png
- x_obsidianmese_chest_inside.png
- x_obsidianmese_axe.png
- x_obsidianmese_pick.png
- x_obsidianmese_pick_engraved.png
- x_obsidianmese_hoe.png
- x_obsidianmese_chest_particle.png
- x_obsidianmese_silver_sand_path_side.png
- x_obsidianmese_shovel.png
- x_obsidianmese_sword_balrog_boss.png
- x_obsidianmese_sword_diamond_engraved.png
- x_obsidianmese_sword.png
- x_obsidianmese_path_overlay_2.png
- x_obsidianmese_path_permafrost_base.png
- x_obsidianmese_dry_dirt_path_side.png
- x_obsidianmese_path_permafrost_side.png
- x_obsidianmese_path_dry_dirt_base.png
- x_obsidianmese_path_dirt_base.png
- x_obsidianmese_path_overlay.png
- x_obsidianmese_snow_path_top.png
- x_obsidianmese_snow_path_side.png
- x_obsidianmese_silver_sand_path_top.png
- x_obsidianmese_shard.png
- x_obsidianmese_sand_path_top.png
- x_obsidianmese_dirt_path_top.png
- x_obsidianmese_dirt_path_side.png
- x_obsidianmese_desert_sand_path_top.png

### Sounds

**Creative Commons License**, https://freesound.org

- x_obsidianmese_apple_eat.2.ogg
- x_obsidianmese_apple_eat.1.ogg
- x_obsidianmese_chest_close.ogg
- x_obsidianmese_chest_open.ogg
- x_obsidianmese_throwing.ogg

## Installation

see: https://wiki.minetest.net/Installing_Mods
