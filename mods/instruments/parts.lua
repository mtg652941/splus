minetest.register_craftitem("instruments:headsheet", {
	description = "Headsheet",
	inventory_image = "headsheet.png",
	wield_image = "headsheet.png"
})

minetest.register_craftitem("instruments:fretboard", {
	description = "Fretboard",
	inventory_image = "fretboard.png",
	wield_image = "fretboard.png"
})

minetest.register_craftitem("instruments:tuning_peg", {
	description = "Tuning Peg",
	inventory_image = "tuning_peg.png",
	wield_image = "tuning_peg.png"
})
